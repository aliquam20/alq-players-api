package org.aliquam.players.api.connector;

import org.aliquam.cum.network.AlqConnector;
import org.aliquam.cum.network.AlqRequestBuilder;
import org.aliquam.cum.network.AlqUrl;
import org.aliquam.players.api.model.AlqPlayer;
import org.aliquam.players.api.model.AlqPlayerIdentifiers;
import org.aliquam.session.api.connector.AlqSessionManager;

import java.util.UUID;

@SuppressWarnings({"unused", "Convert2Diamond"})
public class AlqPlayersConnector {
    private static final AlqUrl serviceUrl = AlqConnector.of("players");
    private static final AlqUrl playerResource = serviceUrl.resolve("player");
    private static final AlqUrl identifiersResource = serviceUrl.resolve("identifiers");

    private final AlqSessionManager sessionManager;

    public AlqPlayersConnector(AlqSessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public AlqPlayerIdentifiers getIdentifiers(AlqPlayerIdentifiers identifiers) {
        return new AlqRequestBuilder(identifiersResource
                .queryParam("internal", identifiers.getInternalId())
                .queryParam("minecraft", identifiers.getMinecraftId())
                .queryParam("discord", identifiers.getDiscordId()))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(AlqPlayerIdentifiers.class);
    }

    public AlqPlayerIdentifiers patchIdentifiers(AlqPlayerIdentifiers identifiers) {
        return new AlqRequestBuilder(identifiersResource
                .queryParam("internal", identifiers.getInternalId()))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .PATCH(identifiers)
                .build().execute(AlqPlayerIdentifiers.class);
    }

    public AlqPlayer createPlayer(AlqPlayer player) {
        return new AlqRequestBuilder(playerResource)
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .POST(player)
                .build().execute(AlqPlayer.class);
    }

    public AlqPlayer getPlayer(UUID id) {
        return new AlqRequestBuilder(playerResource.resolve(id))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .GET()
                .build().execute(AlqPlayer.class);
    }

    public AlqPlayer patchPlayer(AlqPlayer player) {
        return new AlqRequestBuilder(playerResource)
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .PATCH(player)
                .build().execute(AlqPlayer.class);
    }

    public void deletePlayer(AlqPlayer player) {
        deletePlayer(player.getInternalId());
    }

    public void deletePlayer(UUID id) {
        new AlqRequestBuilder(playerResource.resolve(id))
                .acceptApplicationJson()
                .includeSession(sessionManager)
                .withRetries()
                .DELETE()
                .build().execute();
    }

}
