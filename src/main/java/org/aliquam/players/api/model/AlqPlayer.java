package org.aliquam.players.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.TimeZone;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlqPlayer {
    UUID internalId;
    LocalDateTime lastOnline;
    String lastName;
    UUID lastServer;
    UUID lastWorld;
    Double balance;
    TimeZone timezone;
    String language;
}
