package org.aliquam.players.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.TimeZone;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlqPlayerIdentifiers {
    private UUID internalId;
    private UUID minecraftId;
    private Long discordId;

    public static AlqPlayerIdentifiers ofInternalId(UUID id) {
        return new AlqPlayerIdentifiers(id, null, null);
    }

    public static AlqPlayerIdentifiers ofMinecraftId(UUID id) {
        return new AlqPlayerIdentifiers(null, id, null);
    }

    public static AlqPlayerIdentifiers ofDiscordId(Long id) {
        return new AlqPlayerIdentifiers(null, null, id);
    }

}
